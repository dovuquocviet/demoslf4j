import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Demo {

    public static void main(String[] args) {
        System.setProperty(org.slf4j.impl.SimpleLogger.LOG_FILE_KEY, "/home/vietdvq/Jmango/Logs/mylogs.log");
        Logger logger = LoggerFactory.getLogger(Demo.class);
        logger.info("Hello World");
    }
}